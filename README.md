# Pors-FizzBuzz


## Build Instructions

### Ubuntu

* Install Stack (see https://docs.haskellstack.org/en/stable/README/)
  + **Recommended**: curl -sSL https://get.haskellstack.org/ | sh
  + **Note:** if you use the ubuntu package to install Stack and then you upgrade stack (which I believe you need to for this project) you will end up with two different Stack installations (:S) The one in ~/.local/bin/stack is the one you want to use

* ~/.local/bin/ will need to be on your path

* Clone git repo: https://gitlab.com/MikePors/FizzBuzzWarz.git

* First time setup:
  + stack setup
  + stack build
  + stack install

* Commands:
  + "stack setup" installs the compiler. Only needed to be run once.
  + "stack build" will build the project
  + "stack test" runs the tests
  + "stack install" installs the executable created by the build command

* The executable will be installed in ~/.local/bin/

* To run: Pors-FizzBuzz-exe

* Note: the game should now be beatable but challenging!  Enjoy!
