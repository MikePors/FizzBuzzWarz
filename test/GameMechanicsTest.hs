module GameMechanicsTest where

import           Test.Hspec
import           Game
import           GameMechanics
import           FizzBuzzCode

mechanicsTests :: IO()
mechanicsTests = hspec $ do
  describe "updateGameBoard" $ do
    testGameNotOver
    testTie
    testHumanWin
    testAiWin
  describe "executeAiTurn" $
    it "damages the human player by 1" $
      executeAiTurn (InPlay (Human 3 4) [AI 2 5] [0..10]) 0 `shouldBe` InPlay (Human 2 4) [AI 2 5] [if i /= 5 then NoInfo i else EnemyHere 5 | i <- [0..10]]
  describe "executeHumanTurn" $ do
    it "damages AI hit by the FizzBuzz spell! Fizzes cause 1 damage. Fizz Seq = 1, 1, 2, 3, 5, 8" $
      executeHumanTurn (InPlay humanPlayer [AI 3 5] [0..10]) (1, -1) `shouldBe` InPlay humanPlayer [AI 2 5] (take 11 fibonacciFizzBuzz)
    it "damages AI hit by the FizzBuzz spell! Buzzes cause 1 damage.  Buzz Seq = 2, 1, 3, 4, 7" $
      executeHumanTurn (InPlay humanPlayer[AI 3 7] [0..10]) (1, -1) `shouldBe` InPlay humanPlayer [AI 2 7] (take 11 fibonacciFizzBuzz)
    it "damages AI hit by the FizzBuzz spell! FizzBuzz does 2 damage." $
      executeHumanTurn (InPlay humanPlayer [AI 4 3] [0..15]) (1, -1) `shouldBe` InPlay humanPlayer [AI 2 3] fibonacciFizzBuzz
    it "causes no damage to an AI not hit by the spell" $
      executeHumanTurn (InPlay humanPlayer [AI 5 6] [0..9]) (1, -1) `shouldBe` InPlay humanPlayer [AI 5 6] (take 10 fibonacciFizzBuzz)
    it "damages each AI hit by the spell" $
      executeHumanTurn (InPlay humanPlayer [AI 2 3, AI 2 8, AI 2 9] [0..15]) (1, -1) `shouldBe` InPlay humanPlayer [AI 0 3, AI 1 8, AI 2 9] fibonacciFizzBuzz
    where humanPlayer = Human 1 0
          fibonacciFizzBuzz = [Fizz 0, FizzBuzz 1, FizzBuzz 2, FizzBuzz 3, Buzz 4, Fizz 5, Norm 6, Buzz 7, Fizz 8, Norm 9, Norm 10, Buzz 11, Norm 12, Fizz 13, Norm 14, Norm 15]


testGameNotOver :: SpecWith ()
testGameNotOver = do
                  it "returns the provided game state if the human player and the AIs are still alive" $
                    updateGameBoard gameState (Human 18 0) [AI 3 0] ['a'] `shouldBe` InPlay (Human 18 0) [AI 3 0] ['a']
                  it "returns the provided game state even if some AI are dead" $
                    updateGameBoard gameStateWithADeadAI (Human 20 0) [AI 0 3, AI 2 9] [0..20] `shouldBe` InPlay (Human 20 0) [AI 0 3, AI 2 9] [0..20]
                  where gameState = InPlay (Human 20 0) [AI 2 13] [0..20]
                        gameStateWithADeadAI = InPlay (Human 20 0) [AI 0 3, AI 2 9] (map Fizz [0..20])
testTie :: SpecWith ()
testTie = it "returns TIE if the human and all AIs have no health" $
            updateGameBoard gameState (Human 0 0) [AI 0 7, AI 0 13] [0..30] `shouldBe` Tie
          where gameState = InPlay (Human 2 0) [AI 1 7, AI 1 13] [0..30]
                boardLength = 30
testHumanWin :: SpecWith ()
testHumanWin = it "returns HumanWins if the human player is alive and all AIs are dead" $ do
                 updateGameBoard initialGameState livingHumanPlayer [deadAi] [0..10] `shouldBe` HumanWins
                 updateGameBoard initialGameState livingHumanPlayer [deadAi, anotherDeadAi] [0..9] `shouldBe` HumanWins
               where livingHumanPlayer = Human 4 4
                     initialGameState = InPlay livingHumanPlayer [AI 0 3, AI 0 8] [0..10]
                     deadAi = AI 0 0
                     anotherDeadAi = AI 0  8

testAiWin  :: SpecWith ()
testAiWin = do
            it "returns AiWins if the human player is dead and the AI is not" $
              updateGameBoard initialGameState deadPlayer [livingAi] "abcdefgh" `shouldBe` AiWins
            it "returns AiWins if the human player is dead so long as at least one AI is alive" $
              updateGameBoard initialGameState deadPlayer [deadAi, livingAi] [0..12] `shouldBe` AiWins
            where deadPlayer = Human 0 0
                  livingAi = AI 3 3
                  deadAi = AI 0 5
                  initialGameState = InPlay (Human 1 0) [livingAi, deadAi] [0..12]
