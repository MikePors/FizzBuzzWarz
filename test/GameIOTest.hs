module GameIOTest where

import           GameIO
import           Test.Hspec

printBoardTests :: IO()
printBoardTests = hspec $
    describe "createPrintableBoard" $
     do
      it "returns a string representation of the game board" $
         createPrintableBoard 3 show [0..2] `shouldBe` ["[0,1,2]"]
      it "returns blank when the board is empty" $
         createPrintableBoard 3 show ([]::[Int]) `shouldBe` []
      it "will display multiple rows when the game board is longer than the specified number of columns" $
         createPrintableBoard 4  show [1..5] `shouldBe` ["[1,2,3,4]","[5]"]
      it "uses the specified function to render each row" $
         createPrintableBoard 4 ((++"\n").map (const '.')) [0..7] `shouldBe` ["....\n","....\n"]
