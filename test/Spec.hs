import           GameIOTest
import           GameTest
import           LucasTest
import           GameMechanicsTest

main :: IO ()
main = do
       printBoardTests
       gameTests
       lucasTests
       mechanicsTests
