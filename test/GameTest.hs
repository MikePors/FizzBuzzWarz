module GameTest where

import           Game
import           Test.Hspec
import           Lucas
import           FizzBuzzCode

gameTests :: IO()
gameTests = hspec $ do
            describe "isGameOver" $ do
              it "returns true on a Tie" $
                isGameOver (Tie::GameState Int) `shouldBe` True
              it "returns true when the player has won" $
                isGameOver (HumanWins::GameState String) `shouldBe` True
              it "returns true when the AIs have one" $
                isGameOver (AiWins::GameState Char) `shouldBe` True
              it "returns false otherwise" $
                isGameOver (InPlay (Human 0 0) [AI 1 1] [0..5]) `shouldBe` False
            describe "isAlive" $ do
              it "returns True when a player has more than 0 health" $
                isAlive (Human 3 0) `shouldBe` True
              it "retuns False when a player has 0 health" $
                isAlive (AI 0 8) `shouldBe` False
              it "returns False when a player has less than 0 health" $
                isAlive (Human (-3) 12) `shouldBe` False
            describe "damagePlayer" $
              it "reduces a players health by the given amount" $
                damagePlayer (Human 8 12) 3 == Human 5 12
            describe "castFizzBuzz" $ do
              it "returns an empty list if the board is empty" $
                castFizzBuzz ([]::[Int]) [1, 2, 3] [2, 3] `shouldBe` [];
              it "wraps all inputs in 'Norm' if the fizz and buzz lists are empty" $
                  castFizzBuzz [1, 2, 3] [] [] `shouldBe` [Norm 1, Norm 2, Norm 3]
              it "inserts 'Fizz' wrapped values from the second into the first and wraps the rest in 'Norm' when the buzz-list is empty" $
                castFizzBuzz [0..8] [2 * i | i <- [0..4]] [] `shouldBe` [Fizz 0, Norm 1, Fizz 2, Norm 3, Fizz 4, Norm 5, Fizz 6, Norm 7, Fizz 8]
              it "returns the board `Norm`d when there are no more fizzes or buzzes" $
                castFizzBuzz [0..6] firstSeq secondSeq `shouldBe` [FizzBuzz 0, Norm 1, Fizz 2, Buzz 3, Fizz 4, Norm 5, FizzBuzz 6]
              it "properly handles multiple elements and improper order" $
                castFizzBuzz [0..10] [0, 1, 1, 2, 3, 5, 8] [2, 1, 3, 4, 7] `shouldBe` fibonacciFizzBuzz
              it "properly integrates with the lucas sequences" $
                castFizzBuzz [0..10] (createLucasUSequence 1 (-1) 10) (createLucasVSequence 1 (-1) 10) `shouldBe` fibonacciFizzBuzz
              it "properly produces sequential sequences" $ do
                map fizzBuzzValue (castFizzBuzz [0..100] (createLucasUSequence 1 1 100) (createLucasVSequence 1 1 100)) `shouldBe` [0..100]
                map fizzBuzzValue (castFizzBuzz [0..10] (createLucasUSequence 2 2 10) (createLucasVSequence 2 2 10)) `shouldBe` [0..10]
                  where firstSeq = take 10 [2 * i | i <- [0..]]
                        secondSeq = take 10 [3 * i | i <- [0..]]
                        fibonacciFizzBuzz = [Fizz 0, FizzBuzz 1, FizzBuzz 2, FizzBuzz 3, Buzz 4, Fizz 5, Norm 6, Buzz 7, Fizz 8, Norm 9, Norm 10]
