module LucasTest where

import           Lucas
import           Test.Hspec

lucasTests :: IO()
lucasTests = hspec $ do
             describe "createLucasUSequence" $ do
               it "accepts values p = 1, q = -1, bound = 10 and returns the fibonacci sequence up to 10" $
                 createLucasUSequence 1 (-1) 10 `shouldBe` [0, 1, 1, 2, 3, 5, 8]
               it "accepts values p = 1, q = 1, bound = 15, and returns the sequence [0, 1, -1, 2, -3, 5, -8]" $
                 createLucasUSequence 1 1 15 `shouldBe` [0, 1, -1, 2, -3, 5, -8, 13, -21]
             describe "createLucasVSequence" $ do
               it "accepts values p = 1, q = -1, bound = 10 and returns [2, 1, 3, 4, 7]" $
                 createLucasVSequence 1 (-1) 10 `shouldBe` [2, 1, 3, 4, 7]
               it "accepts values p = 1, q = 1, bound = 10, and returns [2, 1, 1, 0, 1, -1, 2, -3, 5, -8]" $
                 createLucasVSequence 1 1 10 `shouldBe` [2, 1, 1, 0, 1, -1, 2, -3, 5, -8]
