module FizzBuzzCode where

data FizzBuzzCode a = Norm a | Fizz a | Buzz a | FizzBuzz a
instance Show a => Show (FizzBuzzCode a) where
  show (Norm i)     = show i
  show (Fizz i)     = "Fizz"
  show (Buzz i)     = "Buzz"
  show (FizzBuzz i) = "FizzBuzz"

instance Eq a => Eq (FizzBuzzCode a) where
  (Norm x) == (Norm y)         = x == y
  (Fizz x) == (Fizz y)         = x == y
  (Buzz x) == (Buzz y)         = x == y
  (FizzBuzz x) == (FizzBuzz y) = x == y
  x == y                       = False


fizzBuzzValue :: FizzBuzzCode a -> a
fizzBuzzValue x = case x of
  Norm i     -> i
  Fizz i     -> i
  Buzz i     -> i
  FizzBuzz i -> i
