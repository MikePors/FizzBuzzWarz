module GameIO where

import           Control.Arrow                as Arrow
import           Control.Monad                (when, replicateM_, unless)
import           Control.Monad.Loops          (iterateUntilM)
import           Data.Maybe                   (isJust, fromJust)
import           System.Random                (random, newStdGen)
import           Game
import           GameMechanics
import           Lucas
import           FizzBuzzCode
import           WindowCommands
import           System.Console.ANSI
import           System.Console.Terminal.Size as Term (Window,
                                                       height,
                                                       size,
                                                       width)
import           Text.Read (readMaybe)

runGame :: IO()
runGame = do
  runOpeningSequence
  initialGameState <- initializeGameState
  iterateUntilM isGameOver doGameLoop initialGameState
  return ()

runOpeningSequence :: IO()
runOpeningSequence = do
  doClearScreen
  scrollPageDown 1
  cursorUpLine 1
  maybeDimensions <- windowDimensions
  when (isJust maybeDimensions) $
    let (height, _) =  fromJust maybeDimensions
    in cursorDownLine height
  printCenterAndClear "You are under attack!"
  printCenterAndClear "Your enemies are hiding!"
  printCenterAndClear "Quickly!  Cast FizzBuzz and try to defeat them before they defeat you!"
  printCenterAndClear "To do this you will need to choose values p and q..."
  printCenterAndClear "Once you do, your spell will generate two sequences satisfying X(n +  2) = p * X(n + 1) - q * X(n)..."
  printCenterAndClear "The first will start with 0 and 1..."
  printCenterAndClear "The second will start with 2 and p."
  printCenterAndClear "'Fizz's and 'Buzz's do one damage each.  `FizzBuzz's do two."
  printCenter "Try it now:"
  (p, q) <- getUserInput
  doClearScreen
  let uSeq = createLucasUSequence p q 51
      vSeq = createLucasVSequence p q 51
  printCenterAndClear $ "First: " ++ show (take 10 uSeq) ++ "..."
  printCenterAndClear $ "Second: " ++ show (take 10 vSeq) ++ "..."
  printCenterAndClear $ "Combine to form: " ++ show (castFizzBuzz [0..20] uSeq vSeq)
  return ()

getUserInput :: IO (Int, Int)
getUserInput =
  do
    putStrLn "Enter a value p:"
    p <- getInt
    putStrLn "Enter a value q:"
    q <- getInt
    return (p, q)

doGameLoop :: GameState Int -> IO (GameState Int)
doGameLoop gs =
      do
      refresh gs
      (p, q) <- getUserInput
      let gameStateAfterHuman = executeHumanTurn gs (p, q)
      refresh gameStateAfterHuman
      if not(isGameOver gameStateAfterHuman)
      then do
         newLine 2
         waitForEnter >> doClearScreen
         printCenterAndClear "Now your opponents will strike from the shadows!  One may reveal themselves!"
         init <- newStdGen
         let (randomSeed, _) = random init
             gameStateAfterAiTurn = executeAiTurn (resetBoard gameStateAfterHuman) randomSeed
         refresh gameStateAfterAiTurn
         unless (isGameOver gameStateAfterAiTurn) $ waitForEnter >> doClearScreen
         return $ resetBoard gameStateAfterAiTurn
      else return $ resetBoard gameStateAfterHuman

showEndScreen :: String -> IO()
showEndScreen msg = fmap (maybe 0 snd) (doClearScreen >> printCenter msg >> windowDimensions) >>= cursorDownLine >> doClearScreen

refresh :: Show a => GameState a -> IO ()
refresh gameState =
  case gameState of
    HumanWins -> showEndScreen "You win!"
    AiWins    -> showEndScreen "Oh no!  You lost!"
    Tie       -> showEndScreen "I didn't think a tie was possible..."
    _         ->
      do
      let human = humanPlayer gameState
          ais = aiPlayers gameState
          humanDisplay = createHumanDisplay human
          aiDisplay = createAiDisplay ais
          board = fizzBuzzifyGameBoard gameState
      doClearScreen
      printSides humanDisplay aiDisplay
      newLine 2
      printBoard 10 show board

newLine :: Int -> IO ()
newLine n = replicateM_ n (putStrLn "")

fizzBuzzifyGameBoard :: GameState a -> [FizzBuzzCode a]
fizzBuzzifyGameBoard gameState = map  Norm $ board gameState

createHumanDisplay :: Player -> String
createHumanDisplay (Human health position) = "Player Health Remaining: " ++ show health
createAiDisplay ais = "AIs Health:" ++ foldr ((++).(" "++).show.playerHealth) "" (filter isAlive ais)

createPrintableBoard :: Show a => Int -> ([a] -> String) -> Board a -> [String]
createPrintableBoard _ _ [] = []
createPrintableBoard cols rowWriter board = rowWriter firstRow : createPrintableBoard cols rowWriter remainder
              where (firstRow, remainder) = splitAt cols board


printBoard :: Show a => Int -> ([a] -> String) -> Board a -> IO()
printBoard cols showFunc board = mapM_ (printCenterHere False) $ createPrintableBoard cols showFunc board
