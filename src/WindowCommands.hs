module WindowCommands where

import           System.Console.ANSI
import           System.Console.Terminal.Size as Term (Window,
                                                       height,
                                                       size,
                                                       width)
import           Text.Read (readMaybe)
import Control.Arrow as Arrow
import Control.Monad (when)


type WindowHeight = Int
type WindowWidth = Int

doClearScreen :: IO()
doClearScreen = clearScreen

getInt :: IO Int
getInt = do
         numStr <- getLine
         let maybeRet = readMaybe numStr :: Maybe Int
         maybe (putStrLn "rly?" >> getInt) return maybeRet

waitForEnter :: IO()
waitForEnter =
  do
    window <- Term.size
    let width = maybe 0 Term.width window
    doWaitForEnter width

doWaitForEnter width =
                do
                  let enterMessage = "[Press 'Enter' to Continue]"
                      padding = quot (width - length enterMessage) 2
                  putStrLn $ replicate padding ' ' ++ enterMessage
                  getLine
                  return ()

printSides :: String -> String -> IO()
printSides left right = do
  maybeDimensions <- windowDimensions
  let requiredSpace = length left + length right
      spacing = maybe 1 ((+(-requiredSpace)).snd) maybeDimensions
  putStrLn $ left ++ replicate spacing ' '  ++ right

windowDimensions :: IO(Maybe(WindowHeight, WindowWidth))
windowDimensions = do
  maybeWindow <- Term.size
  return $ fmap (Term.height Arrow.&&& Term.width) maybeWindow

printCenterHereAndClear :: String -> IO ()
printCenterHereAndClear msg = printCenterHere True msg >> doClearScreen

printCenterHere :: Bool -> String -> IO()
printCenterHere shouldWaitForEnter message =
  do
    window <- Term.size
    let width = maybe 0 Term.width window
        -- messageString = show message
        leftPadding = max 0 (width - length message) `quot` 2
    putStrLn $ replicate leftPadding ' ' ++ message
    when shouldWaitForEnter $ doWaitForEnter width >> doClearScreen

printCenter :: String -> IO ()
printCenter = doPrintCenter False

printCenterAndClear :: String -> IO ()
printCenterAndClear = doPrintCenter True

doPrintCenter :: Bool -> String -> IO ()
doPrintCenter shouldClear message =
                  do
                  window <- Term.size
                  let height = maybe 0 Term.height window
                      width = maybe 0 Term.width window
                      leftPadding = (width - length message) `quot` 2
                      lowerPadding = height `quot` 2
                  cursorDownLine lowerPadding
                  cursorUpLine lowerPadding
                  putStrLn $ replicate leftPadding ' ' ++ message
                  doWaitForEnter width
                  when shouldClear doClearScreen
                  return ()
