module Game where

import           Data.List  (sort)
import           Data.Maybe (fromJust, isNothing)
import           FizzBuzzCode

type Board a = [a]

data Clue a = NoInfo a | EnemyHere a deriving Eq

instance Show a => Show (Clue a) where
  show (NoInfo x) = show x
  show (EnemyHere x) = "Enemy!"
data Player = Human { playerHealth :: Int, playerPosition :: Int } | AI {playerHealth :: Int, playerPosition :: Int} deriving (Eq, Show)
data GameState a = InPlay { humanPlayer :: Player, aiPlayers :: [Player], board :: [a]} | HumanWins | AiWins | Tie deriving (Eq, Show)

isAlive :: Player -> Bool
isAlive = (>0).playerHealth

isGameOver :: Eq a => GameState a -> Bool
isGameOver gs = gs `elem` [HumanWins, AiWins, Tie]

damagePlayer :: Player -> Int -> Player
damagePlayer player amt = player {playerHealth = max (playerHealth player - amt) 0}

castFizzBuzz :: (Eq a, Ord a, Show a) => Board a -> [a] -> [a] -> [FizzBuzzCode a]
castFizzBuzz xs fs bs = let scrubbedXs = clean xs
                            scrubbedFs = clean fs
                            scrubbedBs = clean bs
                        in performCast scrubbedXs scrubbedFs scrubbedBs
                        where clean [] = []
                              clean zs = let (first:sortedZs) = sort zs
                                         in sort $ foldr (\z (w:ws) -> if z == w then w:ws else z:w:ws) [first] sortedZs
performCast :: (Eq a, Ord a, Show a) => [a] -> [a] -> [a] -> [FizzBuzzCode a]
performCast xs [] [] = map Norm xs
performCast [] _ _ = []
performCast (n:board) [] (b:buzzList)
    | n < b = Norm n:performCast board [] (b:buzzList)
    | otherwise = Buzz b:performCast board [] buzzList
performCast (n:board) (f:fizzList) []
    | n < f = Norm n:performCast board (f:fizzList) []
    | otherwise = Fizz f:performCast board fizzList []
performCast (n:board) (f:fizzList) (b:buzzList)
    | n < f && n < b = Norm n:performCast board (f:fizzList) (b:buzzList)
    | n > f = performCast (n:board) fizzList (b:buzzList)
    | n > b = performCast (n:board) (f:fizzList) buzzList
    | f == b && n == f = FizzBuzz f:performCast board fizzList buzzList
    | f == b && n > f = performCast (n:board) fizzList buzzList
    | f < b = Fizz f:performCast board fizzList (b:buzzList)
    | b < f = Buzz b:performCast board (f:fizzList) buzzList
