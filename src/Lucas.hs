module Lucas where

createLucasUSequence :: Int -> Int -> Int -> [Int]
createLucasUSequence = createLucasSequence 0 1

createLucasVSequence :: Int -> Int -> Int -> [Int]
createLucasVSequence p = createLucasSequence 2 p p

createLucasSequence :: Int -> Int -> Int -> Int -> Int -> [Int]
createLucasSequence x0 x1 p q bound =
                                let seq = x0:x1:zipWith (curry (\(x, y) -> p*x - q*y)) seq (drop 1 seq)
                                -- in map fst $ takeWhile ((<=bound).fst) $ zip seq [0..]
                                in map fst $ takeWhile (\(s, i) -> s <= bound && i <= 10 * bound) $ zip seq [0..]
